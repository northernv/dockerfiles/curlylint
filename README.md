# [Curylint](https://www.curlylint.org/) for [NunJucks](https://mozilla.github.io/nunjucks/templating.html)


## Usage

* Need to mount template file under `/check`
* Override config with `pyproject.toml` file [docs](https://www.curlylint.org/docs/configuration)

```
docker run -it --rm --mount src="$(pwd)",target=/check,type=bind registry.gitlab.com/northernv/dockerfiles/curlylint:latest
```


