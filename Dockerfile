FROM python:3-alpine
LABEL maintainer="shane@northernv.com"
LABEL org.opencontainers.image.source="https://gitlab.com/northernv/dockerfiles/curlylint.git"

RUN pip install curlylint

WORKDIR /check

CMD curlylint --include .njk /check
